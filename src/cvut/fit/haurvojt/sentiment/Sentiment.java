/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvut.fit.haurvojt.sentiment;

import cvut.fit.haurvojt.sentiment.ImdbRetriever.Unit;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.log4j.BasicConfigurator;

/**
 *
 * @author haurvojt
 */
public class Sentiment {
    
    private static String webpage = null;
    private static int commentCount = -1;
    private static boolean vectors = false;

    private static void printHelp() {
        System.out.println("Usage:\n -h prints this help"
                + "\n -w <movie-id> Movie identifier in the url, e.q. tt2802144 (required param)"
                + "\n -c <int> count of comments to be read (required)"
                + "\n -v give vector output rather than summarized (optional)");
    }

    public static void parse(String[] args) {
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-h": {
                    printHelp();
                    System.exit(0);
                }
                case "-w": {
                    i++;
                    webpage = args[i];
                    break;
                }
                case "-c":{
                    i++;
                    try{
                        
                        commentCount = Integer.parseInt(args[i]);
                        if(commentCount <=0){
                            throw new NumberFormatException();
                        }
                    } catch (NumberFormatException nfe){
                        System.err.println("Wrong -c parameter. Exiting.");
                        System.exit(1);
                    }
                    break;
                }
                case "-v":{
                    vectors = true;
                    break;
                }
                default:{
                    printHelp();
                    System.exit(1);
                }
            }

        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        List<Unit> units;

        parse(args);
        try {
            units = ImdbRetriever.load(webpage, commentCount);
        } catch (IOException ex) {
            System.err.println("Cannot read from the URL.");
            return;
        }
        
        
        
        List<Integer> originalOpinions = ImdbRetriever.processVector(units);
        List<Integer> stanford = SentimentAnalyzer.processVector(units);
        List<Integer> sentiWordNet;
        SentiWordNet swn;
        try {
            swn = new SentiWordNet("SentiWordNet_3.0.0_20130122.txt");
            sentiWordNet = swn.processVector(units);
        } catch (IOException | ResourceInstantiationException | ExecutionException | InstantiationException ex) {
            System.err.println("SentiWordNet failed. Exiting");
            return;
        }
        
        if(vectors){
            for(int i = 0; i < units.size(); i++){
                System.out.println(units.get(i).title + ", " + originalOpinions.get(i) + ", " + stanford.get(i) + ", " + sentiWordNet.get(i));
                
            }
        }
        else{
            double opinionR = 0, stanR = 0, swnR = 0;
            double distStan = 0, distSwn = 0, distCls = 0;
            for(int i = 0; i < units.size(); i++){
                opinionR += originalOpinions.get(i);
                stanR += stanford.get(i);
                swnR += sentiWordNet.get(i);
                distStan += Math.abs(originalOpinions.get(i) - stanford.get(i));
                distSwn += Math.abs(originalOpinions.get(i) - sentiWordNet.get(i));
                distCls += Math.abs(stanford.get(i) - sentiWordNet.get(i));
                
            }
            opinionR /= units.size();
            stanR /= units.size();
            swnR /= units.size();
            distStan /= units.size();
            distSwn /= units.size();
            distCls /= units.size();
            
            System.out.println("Star ratings: " + opinionR + " Stanford semantic analysis: " + stanR + " SentiWordNet: " + swnR +
                    " dist Stanford " + distStan + " distance SWN " + distSwn + " Distance Classifiers " + distCls );
        }
        
        



    }

}
