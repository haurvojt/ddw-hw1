/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvut.fit.haurvojt.sentiment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author haurvojt
 */
public class ImdbRetriever {

    public static List<Unit> load(String pageCode, int reviewCount) throws IOException {

        List<Unit> units = new ArrayList<>();

        
        for (int i = 0; i <= reviewCount + 10; i += 10) {
            int gain = 0;
            System.out.println("Fetching: http://www.imdb.com/title/" + pageCode + "/reviews?start=" + i);
            URL imdb = new URL("http://www.imdb.com/title/" + pageCode + "/reviews?start=" + i);

            BufferedReader reader = new BufferedReader(new InputStreamReader(imdb.openStream()));

            String line, title = null;
            boolean small = false;
            int fromSmall = 0;
            while ((line = reader.readLine()) != null) {

                if (line.contains("img class=\"avatar\"") ) {
                    small = true;
                    fromSmall = 1;
                }

                if (small && fromSmall == 2) {
                    if (line.startsWith("<h2>") && line.endsWith("</h2>")) {
                        title = line.substring("<h2>".length(), line.length() - "</h2>".length());
                    } else {
                        small = false;
                    }
                }
                if (small && fromSmall == 3) {
                    Pattern pattern = Pattern.compile(".*alt=\"(\\d+)/10\".*");
                    Matcher matcher = pattern.matcher(line);
                    if (matcher.matches()) {
                        int rating = Integer.parseInt(matcher.group(1));

                        if(units.size() < reviewCount){
                            units.add(new Unit(title, rating));
                            gain++;
                        }
                        else{
                            reader.close();
                            return units;
                        }

                    }
                }

                fromSmall++;
            }
            
            
            
            reader.close();
            if(gain == 0){
                return units;
            }
        }

        return units;
    }

    public static List<Integer> processVector(List<Unit> units){
        List<Integer> opinions = new ArrayList<>();
        for(Unit u : units){
            opinions.add(Integer.valueOf((u.rating - 1) / 2 ));
        }
        
        return opinions;
    }
    
    public static class Unit {

        public final String title;
        public final int rating;

        public Unit(String title, int rating) {
            this.title = title;
            this.rating = rating;
        }

        @Override
        public String toString() {
            return "Title " + title + " rating " + rating + "/10";
        }

    }

}
