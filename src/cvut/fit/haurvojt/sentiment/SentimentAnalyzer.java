/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cvut.fit.haurvojt.sentiment;

import cvut.fit.haurvojt.sentiment.ImdbRetriever.Unit;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author haurvojt
 */
public class SentimentAnalyzer {
    
    
    public static int analyze(String sentence){
        
        // initialize the analyzer with tokenizer, sentence splitter and POS for sentiment analysis
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        
        int sentenceSentiment = 0;
        if (sentence != null && sentence.length() > 0) {
            int longest = 0;
            Annotation annotation = pipeline.process(sentence);
            for (CoreMap coreMap : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
                Tree tree = coreMap.get(SentimentCoreAnnotations.AnnotatedTree.class);

                
                int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
                String partText = coreMap.toString();
                if (partText.length() > longest) {
                    sentenceSentiment = sentiment;
                    longest = partText.length();
                }

            }
        }
        
        return sentenceSentiment;
        
    }
    
    
    private static void printTree(Tree tree, int level){
        for(int i = 0; i < level; i++){
            System.out.print("  ");
        }
        try{
            System.out.println(tree + "  " + sentimentDescription(RNNCoreAnnotations.getPredictedClass(tree)));
        } catch (NullPointerException npe){
            return;
        }
        if(!tree.isLeaf()){
            for(Tree child : tree.getChildrenAsList())
                printTree(child, level + 1);
        }
        
    }
    
    public static String sentimentDescription(int sentiment){
        switch(sentiment){
            case 0: return "Very bad";
            case 1: return "Bad";
            case 2: return "Neutral";
            case 3: return "Good";
            case 4: return "Very Good";
            default: return "Not a sentiment value.";
        }
    }
    
    public static List<Integer> processVector(List<Unit> units){
        List<Integer> vector = new ArrayList<>();
        
        for(Unit unit : units){
            vector.add(Integer.valueOf(analyze(unit.title)));
        }
        
        return vector;
    }
    
}
