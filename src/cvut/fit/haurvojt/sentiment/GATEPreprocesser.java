/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvut.fit.haurvojt.sentiment;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.ProcessingResource;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author haurvojt
 */
public class GATEPreprocesser {

    private static final String WORD = "word";
    private static final String KIND = "kind";
    private static final String STRING = "string";

    private ProcessingResource tokenizerPR;
    private SerialAnalyserController annotationPipeline;

    public GATEPreprocesser() throws InstantiationException {
        init();
        try {
            tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            annotationPipeline.add(tokenizerPR);
        } catch (ResourceInstantiationException ex) {
            throw new InstantiationException(ex.getMessage());
        }
    }

    public List<String> tokenize(String str) throws ResourceInstantiationException, ExecutionException {

        List<String> words = new ArrayList<>();

        Corpus corpus = Factory.newCorpus("corpus");

        Document doc = Factory.newDocument(str);
        corpus.add(doc);

        annotationPipeline.setCorpus(corpus);

        annotationPipeline.execute();

        FeatureMap featureMap = null;
        AnnotationSet asTokens = doc.getAnnotations().get("Token", featureMap);
        List tokensList = new ArrayList(asTokens);

        for (int i = 0; i < tokensList.size(); i++) {
            Annotation token = (Annotation) tokensList.get(i);
            featureMap = token.getFeatures();

            if (featureMap.get("kind").equals(WORD)) {
                // any object has toString() method, for String it is identity, so it is safer to call like this rather than using conversion
                words.add(featureMap.get(STRING).toString());
            }
        }

        corpus.remove(doc);

        return words;

    }

    private void init() throws InstantiationException {

        try {

            Gate.setGateHome(new File("/Applications/GATE_Developer_8.0"));
            File pluginsHome = new File("/Applications/GATE_Developer_8.0/plugins");
            Gate.setPluginsHome(pluginsHome);

            // initialise the GATE library
            Gate.init();

            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURI().toURL();
            register.registerDirectories(annieHome);

        } catch (MalformedURLException | GateException ex) {
            throw new InstantiationException("Couldn't initialize GATE");
        }
    }

}
